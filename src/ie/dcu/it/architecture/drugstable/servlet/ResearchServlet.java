package ie.dcu.it.architecture.drugstable.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import ie.dcu.it.architecture.drugstable.GoogleHelper;
import ie.dcu.it.architecture.drugstable.web.WebPageMapper;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(value = "/research", loadOnStartup = 1)
public class ResearchServlet extends HttpServlet {

	private static final long serialVersionUID = 5551714194234649092L;

	private final GoogleHelper helper;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResearchServlet() {
		super();
		this.helper = GoogleHelper.getInstance();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().print(WebPageMapper.getHTMLFrom("pages/research.html", request.getContextPath()));
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		final String cis = request.getParameter("cis");
		final String contains = request.getParameter("contains");
		String origin;

		if (cis != null && contains != null && !cis.isEmpty() && !contains.isEmpty()) {
			// Look by both
			origin = new ObjectMapper().writeValueAsString(this.helper.getDrugsListByBoth(cis, contains));
		} else if ((cis == null || cis.isEmpty()) && contains != null && !contains.isEmpty()) {
			// Look by regexp
			origin = new ObjectMapper().writeValueAsString(this.helper.getDrugsListByName(contains));
		} else if (cis != null && !cis.isEmpty() && (contains == null || contains.isEmpty())) {
			// Look by cis
			origin = new ObjectMapper().writeValueAsString(this.helper.getDrugsListByCIS(cis));
		} else {
			response.getWriter().println("No CIS or NAME provided");
			return;
		}

		origin = origin.replace("é", "&eacute;");
		origin = origin.replace("è", "&egrave;");
		origin = origin.replace("à", "&agrave;");

		response.getWriter().println(origin);

	}

}
