package ie.dcu.it.architecture.drugstable.servlet;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(value = "/resource", loadOnStartup = 1)
public class ResourceServlet extends HttpServlet {

	private static final long serialVersionUID = 5551714194234649092L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResourceServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String rsc = request.getParameter("name");

		if (rsc != null && !rsc.contains("client_secrets.json")) {
			final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(rsc);

			String type;
			final int dot = rsc.lastIndexOf('.');
			switch (rsc.substring(dot >= 0 ? dot : 0)) {
			case ".css":
				type = "text/css";
				break;
			case ".png":
				type = "image/png";
				break;
			case ".js":
				type = "text/javascript";
				break;
			case ".html":
				type = "text/html";
				break;
			default:
				type = "text/plain";
				break;
			}

			response.setHeader("Content-Type", type);

			if (is != null) {
				IOUtils.copyLarge(is, response.getOutputStream());
				is.close();
			} else {
				response.getOutputStream().print("Unable to found resource : " + rsc);
			}

			response.getOutputStream().flush();
		}
	}

}
