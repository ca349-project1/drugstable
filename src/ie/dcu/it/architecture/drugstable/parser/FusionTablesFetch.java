/*
 * Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package ie.dcu.it.architecture.drugstable.parser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.fusiontables.Fusiontables;
import com.google.api.services.fusiontables.Fusiontables.Query.SqlGet;
import com.google.api.services.fusiontables.FusiontablesScopes;
import com.google.api.services.fusiontables.model.Table;
import com.google.api.services.fusiontables.model.TableList;

public class FusionTablesFetch {

	/**
	 * Be sure to specify the name of your application. If the application name
	 * is {@code null} or blank, the application will log a warning. Suggested
	 * format is "MyCompany-ProductName/1.0".
	 */
	private static final String APPLICATION_NAME = "JavaApp";

	/** Directory to store user credentials. */
	private static final File DATA_STORE_DIR = new File(System.getProperty("java.io.tmpdir"),
			"drugstable/store/fusion_tables_sample");

	/**
	 * Global instance of the {@link DataStoreFactory}. The best practice is to
	 * make it a single globally shared instance across your application.
	 */
	private static FileDataStoreFactory dataStoreFactory;

	/** Global instance of the HTTP transport. */
	private static HttpTransport httpTransport;

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private static Fusiontables fusiontables;

	/** Authorizes the installed application to access user's protected data. */
	private Credential authorize() throws Exception {
		// load client secrets
		final GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(
				Thread.currentThread().getContextClassLoader().getResourceAsStream("/client_secrets.json")));
		if (clientSecrets.getDetails().getClientId().startsWith("Enter")
				|| clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
			System.out.println("Enter Client ID and Secret from https://code.google.com/apis/console/?api=fusiontables "
					+ "into fusiontables-cmdline-sample/src/main/resources/client_secrets.json");
			System.exit(1);
		}
		// set up authorization code flow
		final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY,
				clientSecrets, Collections.singleton(FusiontablesScopes.FUSIONTABLES))
						.setDataStoreFactory(dataStoreFactory).build();
		// authorize
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	public FusionTablesFetch() {
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
			// authorization
			final Credential credential = this.authorize();
			// set up global FusionTables instance
			fusiontables = new Fusiontables.Builder(httpTransport, JSON_FACTORY, credential)
					.setApplicationName(APPLICATION_NAME).build();
			// run commands
			this.listTables();
			// success!
			return;
		} catch (final IOException e) {
			System.err.println(e.getMessage());
		} catch (final Throwable t) {
			t.printStackTrace();
		}
	}

	/** List tables for the authenticated user. */
	private void listTables() throws IOException {
		View.header("Listing Tables and loading them");

		// Fetch the table list
		final Fusiontables.Table.List listTables = fusiontables.table().list();
		final TableList tablelist = listTables.execute();

		if (tablelist.getItems() == null || tablelist.getItems().isEmpty()) {
			System.out.println("No tables found!");
			return;
		}
		for (final Table table : tablelist.getItems()) {
			View.show(table);
			View.separator();
		}
	}

	/**
	 * @param <U>
	 * @param tableId
	 * @param clazz
	 * @throws IOException
	 */
	public <U> U extractDataFromTable(String tableId, Class<U> clazz) throws IOException {
		System.out.println("SELECT * FROM " + (tableId != null ? tableId : "null"));
		final SqlGet sql = fusiontables.query().sqlGet("SELECT * FROM " + tableId);
		try {
			final InputStream resp = sql.executeAsInputStream();

			return new ObjectMapper().readValue(resp, clazz);
			// Parse InputStream
		} catch (final IllegalArgumentException e) {
		}
		return null;
	}

}
