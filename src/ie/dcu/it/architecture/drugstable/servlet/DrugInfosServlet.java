package ie.dcu.it.architecture.drugstable.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import ie.dcu.it.architecture.drugstable.GoogleHelper;
import ie.dcu.it.architecture.drugstable.web.WebPageMapper;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet(value = "/drug", loadOnStartup = 1)
public class DrugInfosServlet extends HttpServlet {

	private static final long serialVersionUID = 1082614847297436214L;

	private final GoogleHelper helper;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DrugInfosServlet() {
		super();
		this.helper = GoogleHelper.getInstance();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().print(WebPageMapper.getHTMLFrom("pages/drug.html", request.getContextPath()));
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String id = request.getParameter("id");
		if (id != null) {
			final long idNbr = Long.parseLong(id);

			String origin = new ObjectMapper().writeValueAsString(this.helper.getDrugItem(idNbr));

			origin = origin.replace("é", "&eacute;");
			origin = origin.replace("è", "&egrave;");
			origin = origin.replace("à", "&agrave;");

			response.getWriter().println(origin);
		} else {
			response.getWriter().println("Missing parameter 'id' => integer");
		}
	}

}
