package ie.dcu.it.architecture.drugstable;

import java.util.List;

public class Page {

	private int current;
	private int total;
	
	private List<Drug> drugs;
	
	/**
	 * @param current
	 * @param total
	 * @param drugs
	 */
	public Page(int current, int total, List<Drug> drugs) {
		super();
		this.current = current;
		this.total = total;
		this.drugs = drugs;
	}

	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(List<Drug> drugs) {
		this.drugs = drugs;
	}
	
}
