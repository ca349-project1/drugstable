package ie.dcu.it.architecture.drugstable;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ie.dcu.it.architecture.drugstable.parser.FusionTablesFetch;

public class GoogleHelper {

	private static GoogleHelper that = null;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	private static final Logger LOG = LoggerFactory.getLogger(GoogleHelper.class);

	private static final int NBR_ELEM_PER_PAGE = 500;

	private FusionTablesFetch ftf = null;
	private int drugsLoaded = 0;
	private List<List<Drug>> drugsListPages = null;

	private GoogleHelper() {
		LOG.info("Loading drugs from Google FusionTable ....");
		this.ftf = new FusionTablesFetch();
		this.setDrugsListPages();
		LOG.info("Drugs loaded : {} | pages created : {} [{} elements/page]", this.drugsLoaded,
				this.drugsListPages.size(), NBR_ELEM_PER_PAGE);
	}

	public static GoogleHelper getInstance() {
		if (that == null) {
			that = new GoogleHelper();
		}
		return that;
	}

	public <U> U getTableFor(String table, Class<U> clazz) {
		try {
			return this.ftf.extractDataFromTable(table, clazz);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public BDPMData getBDPMData() {
		return this.getTableFor(BDPMData.TABLE_NAME, BDPMData.class);
	}

	public DrugsMetaData getDrugsMetaData() {
		return this.getTableFor(DrugsMetaData.TABLE_NAME, DrugsMetaData.class);
	}

	private void setDrugsListPages() {

		final List<Drug> list = new ArrayList<>();
		final BDPMData bdpm = this.getBDPMData();
		final DrugsMetaData meta = this.getDrugsMetaData();
		Long id = 0L;

		for (final List<String> elem : bdpm.getRows()) {
			final Drug drug = this.getDrugFor(elem, meta.getRows());
			if (drug != null) {
				drug.setId(id++);
				list.add(drug);
			}
		}
		this.drugsLoaded = list.size();
		this.drugsListPages = this.getPages(list, NBR_ELEM_PER_PAGE);
	}

	/*
	 * "Code_CIS", "Name", "Pharmaceutical_form", "Administration_methods",
	 * "Administrative_Status", "Type_of_marketing_authorization_procedure",
	 * "State _of_commercialization", "Date_of_marketing_authorization",
	 * "StatutBdm", "European_authorization_number", "Holders",
	 * "Enhanced_surveillance" "Denomination_de_la_specialite",
	 * "Titulaire_de_l'AMM", "CIP_7", "CIP_13", "Nom_de_la_Presentation"
	 */
	private Drug getDrugFor(List<String> elem, List<List<String>> rows) {
		final Drug drug = new Drug();

		if (elem.size() == 12) {
			drug.setCis(Long.parseLong(elem.get(0)));
			drug.setName(elem.get(1));
			drug.setForm(elem.get(2));
			drug.setWay(Arrays.asList(elem.get(3).split(";")));
			drug.setStatus(elem.get(4));
			drug.setType(elem.get(5));
			drug.setState(elem.get(6));
			try {
				drug.setDate(sdf.parse(elem.get(7)));
			} catch (final ParseException e) {
				drug.setDate(null);
				e.printStackTrace();
			}
			drug.setStatusBdm(elem.get(8));
			drug.setAuthorization(elem.get(9));
			drug.setOwners(Arrays.asList(elem.get(10).split(";")));
			drug.setGuarded(elem.get(11).equalsIgnoreCase("Oui"));
		} else {
			return null;
		}
		final List<String> meta = this.getMetaFor(rows, elem.get(0));

		if (meta != null) {
			drug.setDenomination(meta.get(1));
			drug.setOwnerAMM(meta.get(2));
			drug.setCip7(Long.parseLong(meta.get(3)));
			drug.setCip13(Long.parseLong(meta.get(4)));
			drug.setPresentationName(meta.get(5));
		}

		return drug;
	}

	private List<String> getMetaFor(List<List<String>> rows, String cis) {
		for (final List<String> list : rows) {
			if (cis.equals(list.get(0))) {
				return list;
			}
		}
		return null;
	}

	public Page getDrugsList(int pageNbr) {
		if (pageNbr < 0 || pageNbr > this.drugsListPages.size()) {
			return null;
		}
		return new Page(pageNbr, this.drugsListPages.size(), this.drugsListPages.get(pageNbr));
	}

	public <T> List<List<T>> getPages(Collection<T> c, Integer pageSize) {
		Integer ps = pageSize;
		if (c == null) {
			return Collections.emptyList();
		}
		final List<T> list = new ArrayList<>(c);
		if (ps == null || ps <= 0 || ps > list.size()) {
			ps = list.size();
		}
		final int numPages = (int) Math.ceil((double) list.size() / (double) ps);
		final List<List<T>> pages = new ArrayList<>(numPages);
		for (int pageNum = 0; pageNum < numPages;) {
			pages.add(list.subList(pageNum * ps, Math.min(++pageNum * ps, list.size())));
		}
		return pages;
	}

	public int getPagesCount() {
		return this.drugsListPages.size();
	}

	public Drug getDrugItem(Long id) {
		for (final List<Drug> list : this.drugsListPages) {
			for (final Drug drug : list) {
				if (drug.getId().equals(id)) {
					return drug;
				}
			}
		}
		return null;
	}

	public int getDrugsCount() {
		return this.drugsLoaded;
	}

	public Page getDrugsListByBoth(String cis, String contains) {
		final List<Drug> list = new ArrayList<>();

		for (final List<Drug> page : this.drugsListPages) {
			for (final Drug drug : page) {
				final String name = drug.getName().toLowerCase();
				final String presName = drug.getPresentationName().toLowerCase();
				final String denomin = drug.getDenomination().toLowerCase();
				final String pattern = contains.toLowerCase();

				if ((name.contains(pattern) || presName.contains(pattern) || denomin.contains(pattern))
						&& Long.toString(drug.getCis()).contains(cis)) {
					list.add(drug);
				}
			}
		}

		return new Page(0, list.size(), list);
	}

	public Page getDrugsListByName(String contains) {
		final List<Drug> list = new ArrayList<>();

		for (final List<Drug> page : this.drugsListPages) {
			for (final Drug drug : page) {
				final String name = drug.getName().toLowerCase();
				final String presName = drug.getPresentationName().toLowerCase();
				final String denomin = drug.getDenomination().toLowerCase();
				final String pattern = contains.toLowerCase();

				if (name.contains(pattern) || presName.contains(pattern) || denomin.contains(pattern)) {
					list.add(drug);
				}
			}
		}

		return new Page(0, list.size(), list);
	}

	public Page getDrugsListByCIS(String cis) {
		final List<Drug> list = new ArrayList<>();

		for (final List<Drug> page : this.drugsListPages) {
			for (final Drug drug : page) {
				if (Long.toString(drug.getCis()).contains(cis)) {
					list.add(drug);
				}
			}
		}

		return new Page(0, list.size(), list);
	}

}
