package ie.dcu.it.architecture.drugstable.web;

import java.io.IOException;

import org.apache.commons.io.IOUtils;

public class WebPageMapper {

	private WebPageMapper() {
		super();
	}

	public static String getHTMLFrom(String page, String path) {
		try {
			final String template = IOUtils.toString(
					Thread.currentThread().getContextClassLoader().getResourceAsStream("pages/template.html"), "UTF-8");
			String content = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(page),
					"UTF-8");

			content = template.replace("${body}", content);
			content = content.replace("${path}", path);

			return content;
		} catch (final IOException e) {
			e.printStackTrace();
			return "Unable to found the requested page";
		}
	}

}
