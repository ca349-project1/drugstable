package ie.dcu.it.architecture.drugstable.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ie.dcu.it.architecture.drugstable.web.WebPageMapper;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(value = "/stats", loadOnStartup = 1)
public class StatsServlet extends HttpServlet {

	private static final long serialVersionUID = 5551714194234649092L;

	private static final Logger LOG = LoggerFactory.getLogger(StatsServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StatsServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().print(WebPageMapper.getHTMLFrom("pages/stats.html", request.getContextPath()));
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

}
