package ie.dcu.it.architecture.drugstable.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ie.dcu.it.architecture.drugstable.GoogleHelper;
import ie.dcu.it.architecture.drugstable.web.WebPageMapper;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(value = "/", loadOnStartup = 1)
public class HomeServlet extends HttpServlet {

	private static final long serialVersionUID = 5551714194234649092L;

	private static final Logger LOG = LoggerFactory.getLogger(HomeServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomeServlet() {
		super();
		LOG.info("Server is UP");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String index = WebPageMapper.getHTMLFrom("pages/index.html", request.getContextPath());
		final StringBuilder pagesNav = new StringBuilder();

		for (int i = 0; i < GoogleHelper.getInstance().getPagesCount(); i++) {
			pagesNav.append("<li><a onclick=\"changePage('");
			pagesNav.append(Integer.toString(i)).append("');\" href=\"#\">");
			pagesNav.append(Integer.toString(i + 1)).append("</a></li>");
		}

		index = index.replace("${pageBar}", pagesNav.toString());

		response.getWriter().print(index);
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

}
