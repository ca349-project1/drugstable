package ie.dcu.it.architecture.drugstable;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "kind", "columns", "rows" })
public class DrugsMetaData {

	@JsonIgnore
	public static final String TABLE_NAME = "1G6OkBRaCARVO2smUu5JFlBn1M9qDhMMBoPcAgBa5";

	@JsonProperty("kind")
	private String kind;
	@JsonProperty("columns")
	private List<String> columns = new ArrayList<>();
	@JsonProperty("rows")
	private List<List<String>> rows = new ArrayList<>();

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public DrugsMetaData() {
	}

	/**
	 *
	 * @param columns
	 * @param kind
	 * @param rows
	 */
	public DrugsMetaData(String kind, List<String> columns, List<List<String>> rows) {
		this.kind = kind;
		this.columns = columns;
		this.rows = rows;
	}

	/**
	 *
	 * @return The kind
	 */
	@JsonProperty("kind")
	public String getKind() {
		return this.kind;
	}

	/**
	 *
	 * @param kind
	 *            The kind
	 */
	@JsonProperty("kind")
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	 *
	 * @return The columns
	 */
	@JsonProperty("columns")
	public List<String> getColumns() {
		return this.columns;
	}

	/**
	 *
	 * @param columns
	 *            The columns
	 */
	@JsonProperty("columns")
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}

	/**
	 *
	 * @return The rows
	 */
	@JsonProperty("rows")
	public List<List<String>> getRows() {
		return this.rows;
	}

	/**
	 *
	 * @param rows
	 *            The rows
	 */
	@JsonProperty("rows")
	public void setRows(List<List<String>> rows) {
		this.rows = rows;
	}

}