package ie.dcu.it.architecture.drugstable.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import ie.dcu.it.architecture.drugstable.GoogleHelper;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet(value = "/list", loadOnStartup = 1)
public class ListServlet extends HttpServlet {

	private static final long serialVersionUID = 1082614847297436214L;

	private final GoogleHelper helper;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListServlet() {
		super();
		this.helper = GoogleHelper.getInstance();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String nbr = request.getParameter("page");
		if (nbr != null) {
			final int pageNbr = Integer.parseInt(nbr);

			String origin = new ObjectMapper().writeValueAsString(this.helper.getDrugsList(pageNbr));

			origin = origin.replace("é", "&eacute;");
			origin = origin.replace("è", "&egrave;");
			origin = origin.replace("à", "&agrave;");

			response.getWriter().println(origin);
		} else {
			response.getWriter().println("Missing parameter 'page' => integer");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

}
