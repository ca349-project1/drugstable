package ie.dcu.it.architecture.drugstable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Drug {

	private static final String UNKNOWN = "UNKNOWN";

	private Long id;

	private Long cis;
	private Long cip7;
	private Long cip13;

	private String name;
	private String presentationName;
	private String form;

	private List<String> way;

	private String status;
	private String type;
	private String state;

	private Date date;
	private String statusBdm;

	private String authorization;
	private List<String> owners;

	private boolean guarded;

	private String denomination;
	private String ownerAMM;

	public Drug() {
		super();
		this.id = 0L;
		this.cis = 0L;
		this.cip13 = 0L;
		this.cip7 = 0L;
		this.name = UNKNOWN;
		this.presentationName = UNKNOWN;
		this.form = UNKNOWN;
		this.way = new ArrayList<>();
		this.status = UNKNOWN;
		this.type = UNKNOWN;
		this.state = UNKNOWN;
		this.date = null;
		this.statusBdm = UNKNOWN;
		this.authorization = UNKNOWN;
		this.owners = new ArrayList<>();
		this.guarded = false;
		this.denomination = UNKNOWN;
		this.ownerAMM = UNKNOWN;
	}

	/**
	 * @return the id
	 */
	public final Long getId() {
		return this.id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public final void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cis
	 */
	public final Long getCis() {
		return this.cis;
	}

	/**
	 * @param cis
	 *            the cis to set
	 */
	public final void setCis(Long cis) {
		this.cis = cis;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public final void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the form
	 */
	public final String getForm() {
		return this.form;
	}

	/**
	 * @param form
	 *            the form to set
	 */
	public final void setForm(String form) {
		this.form = form;
	}

	/**
	 * @return the way
	 */
	public final List<String> getWay() {
		return this.way;
	}

	/**
	 * @param way
	 *            the way to set
	 */
	public final void setWay(List<String> way) {
		this.way = way;
	}

	/**
	 * @return the status
	 */
	public final String getStatus() {
		return this.status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public final void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the type
	 */
	public final String getType() {
		return this.type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public final void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the state
	 */
	public final String getState() {
		return this.state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public final void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the date
	 */
	public final Date getDate() {
		return this.date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public final void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the statutBdm
	 */
	public final String getStatusBdm() {
		return this.statusBdm;
	}

	/**
	 * @param statusBdm
	 *            the statusBdm to set
	 */
	public final void setStatusBdm(String statusBdm) {
		this.statusBdm = statusBdm;
	}

	/**
	 * @return the authorization
	 */
	public final String getAuthorization() {
		return this.authorization;
	}

	/**
	 * @param authorization
	 *            the authorization to set
	 */
	public final void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	/**
	 * @return the owners
	 */
	public final List<String> getOwners() {
		return this.owners;
	}

	/**
	 * @param owners
	 *            the owners to set
	 */
	public final void setOwners(List<String> owners) {
		this.owners = owners;
	}

	/**
	 * @return the guarded
	 */
	public final boolean isGuarded() {
		return this.guarded;
	}

	/**
	 * @param guarded
	 *            the guarded to set
	 */
	public final void setGuarded(boolean guarded) {
		this.guarded = guarded;
	}

	/**
	 * @return the presentationName
	 */
	public final String getPresentationName() {
		return this.presentationName;
	}

	/**
	 * @param presentationName
	 *            the presentationName to set
	 */
	public final void setPresentationName(String presentationName) {
		this.presentationName = presentationName;
	}

	/**
	 * @return the denomination
	 */
	public final String getDenomination() {
		return this.denomination;
	}

	/**
	 * @param denomination
	 *            the denomination to set
	 */
	public final void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	/**
	 * @return the ownerAMM
	 */
	public final String getOwnerAMM() {
		return this.ownerAMM;
	}

	/**
	 * @param ownerAMM
	 *            the ownerAMM to set
	 */
	public final void setOwnerAMM(String ownerAMM) {
		this.ownerAMM = ownerAMM;
	}

	public Long getCip7() {
		return this.cip7;
	}

	public void setCip7(Long cip7) {
		this.cip7 = cip7;
	}

	public Long getCip13() {
		return this.cip13;
	}

	public void setCip13(Long cip13) {
		this.cip13 = cip13;
	}

}
